#!/bin/bash -eu

# for f in *; do ffprobe "$f" 2>> stats ; done
# grep -E 'Input|bitrate' stats

#for raw in *.MOV
for raw in *.[Aa][Vv][Ii]
do

  # x264
	# http://trac.ffmpeg.org/wiki/Encode/H.264
	out="/tmp/${raw/.[Aa][Vv][Ii]/.mp4}"
	# out="/tmp/${raw/.MOV/.mp4}"
	ffmpeg -i "$raw" -deinterlace \
	-c:v libx264 -b:v 2600k -pass 1 -c:a aac -b:a 64k -f mp4 -y /dev/null &&
  ffmpeg -i "$raw" -deinterlace \
	-c:v libx264 -b:v 2600k -pass 2 -c:a aac -b:a 64k -y "$out"

	# dvd
	# out="/tmp/${raw/.AVI/.mp2}"
	#	ffmpeg -i "$raw" -deinterlace -pass 1 -an -target dvd -y /dev/null
	# ffmpeg -i "$raw" -deinterlace -pass 2 -target dvd -y "$out"

	# vp9
	# out="/tmp/${raw/.AVI/.webm}"
	# ffmpeg -i "$raw" -deinterlace -c:v libvpx-vp9 -pass 1 -b:v 0 -crf 16 \
	# 	-threads 8 -speed 4 \
	# 	-tile-columns 6 -frame-parallel 1 \
	# 	-an -f webm -y /dev/null
	#
	# ffmpeg -i "$raw" -deinterlace -c:v libvpx-vp9 -pass 2 -b:v 0 -crf 16 \
	# 	-threads 8 -speed 1 \
	# 	-tile-columns 6 -frame-parallel 1 -auto-alt-ref 1 -lag-in-frames 25 \
	# 	-c:a libopus -b:a 64k -f webm -y "$out"

	echo transcoded $raw to $out
	ls -l "$raw" "$out"

done
