How To Install Docker
===

see also: https://get.docker.com/

On Fedora 24:

```shell
sudo dnf install docker
sudo systemctl enable docker
sudo groupadd docker
sudo usermod -aG docker $USER
sudo reboot
```

On CentOS 7:

```shell
# ensure that the EPEL repo is available: `yum intall epel-release` or atlas equivalent `atlas-client j 14425`
sudo yum install docker
sudo systemctl enable docker
sudo groupadd docker
sudo usermod -aG docker $USER
sudo reboot
```

On Amazon Linux AMI 2016.09:

```shell
sudo yum install docker
sudo usermod -aG docker $USER
sudo reboot
```

On Ubuntu Trusty or Xenial:

```shell
# see also:
# - https://docs.docker.com/engine/installation/linux/ubuntulinux/
# - DNS issues:http://askubuntu.com/questions/475764/docker-io-dns-doesnt-work-its-trying-to-use-8-8-8-8
CODENAME=$( lsb_release -c | awk '{ print $2 }')
echo "deb https://apt.dockerproject.org/repo ubuntu-$CODENAME main" | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo apt-get update
sudo apt-get purge lxc-docker docker
sudo apt-get autoremove
sudo apt-cache policy docker-engine
sudo apt-get install docker-engine
sudo groupadd docker
sudo usermod -aG docker $USER
# IMPORTANT: edit /etc/default/docker for docker guest DNS settings or disable dnsmasq in NetworkManager
sudo reboot
```
