# Books David L. Willson likes

** Bolded books were suggested to the Feast Mob **

Books for people who are self-editing

- **Atomic Habits by James Clear**
- The Power of Habit by Charles Duhigg

Books with numbers in their titles

- **The One Thing by Keller and Papasan**
- **The Four Agreements by Ruiz**
- **The Seven Habits of Highly Effective People by Covey**
- The 48 Laws of Power by Greene

Books about thinking and learning

- **Thinking Fast and Slow by Kahneman**
- **Powerful Teaching by Agarwal and Bain**

Books about my work

- Free as in Freedom (2002) by Williams
- Just For Fun (2001) by Diamond
- The Book of Satoshi (2014) by Satoshi, Champagne, and Finney
- The Phoenix Project (2014) by Kim, Behr, et al
- The DevOps Handbook (2016) by Kim, Humble, et al
- The SRE Handbook (2016) by Beyer, Murphy, et al
- Accelerate (2018) by Forsgren, Humble, Kim
- Continuous Delivery (2010) by Humble and Farley

Books about business, economics, and management

- The Great Game of Business by Stack and Burlingham
- The New Economics by Demming
- The Book of Satoshi (2014) by Satoshi, Champagne, and Finney
- Human Action by Mises

Books about violent monopolies and how to oppose them

- **The Problem of Political Authority by Huemer**
- **Healing Our World by Ruwart**
- The Law (1850) by Bastiat
- The Book of Satoshi (2014) by Satoshi, Champagne, and Finney

Books that are fun to read

- The Lord of The Rings by Tolkien
- The Chronicles of Narnia by Lewis
- Daemon, Freedom, Influx, Change Agent, and everything else by Daniel Suarez
- The Culture series by Banks
- The Swords and Berserker series' by Saberhagen
- The Ringworld series by Niven
- The Black Company series by Cook
- Small Gods (1992) by Pratchett
- The Eyes and the Impossible (2023) by Eggers
- Watership Down (1972) by Adams
- Running With Sherman (2019) by McDougal

Books I haven't read but want to read

- Flow by Csikszentmihalyi
- "Man, Economy, and State" and For a New Liberty by Rothbard
- The Machinery of Freedom by Friedman
- The Most Dangerous Superstition by Rose
