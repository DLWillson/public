Name: ubrb
Version: 1.0
Release: 1%{?dist}
Summary: Ugly Baby performs rsync backups
License: GPL
URL: https://gitlab.com/DLWillson/dlw-public/-/tree/master/rsync_backup/
Source0: https://thegeek.nu/ubrb-1.0.tar.gz

BuildArch: noarch

%description
Ugly Baby rsync backup is a utility that uses cp, cron, and rsync to back things up on a schedule.


%prep
%setup -q


%install
mkdir -p %{buildroot}/usr/local/bin
install -m 755 bin/ubrb %{buildroot}/usr/local/bin/
mkdir -p %{buildroot}/usr/local/lib/ubrb
cp -r lib/ubrb/* %{buildroot}/usr/local/lib/ubrb/


%files
/usr/local/bin/ubrb
/usr/local/lib/ubrb/*
