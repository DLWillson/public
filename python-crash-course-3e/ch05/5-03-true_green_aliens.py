#!/bin/env python3

def shoot_an_alien(alien):
    if alien['color'] == "green":
        print("5 points!")
    else:
        print(f"No points for a {alien['color']} alien. Try again.")

aliens=[
    {'color': 'red','points': 10},
    {'color': 'blue'},
    {'color': 'green'},
    {'color': 'yellow'}
    ]

for alien in aliens:
    shoot_an_alien(alien)