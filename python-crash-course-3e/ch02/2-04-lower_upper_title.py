#!/usr/bin/env python3

first_name='david'
last_name='willson'
full_name=f'{first_name} {last_name}'
print("lowercase:", full_name.lower())
print("uppercase:", full_name.upper())
print("titlecase:", full_name.title())