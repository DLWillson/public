#!/usr/bin/env python3

long_quote = """

The concept of the true nature of the self is a complex and philosophical topic that has been debated by scholars and thinkers for centuries. Different philosophical and religious traditions have varying perspectives on this matter. Some argue that the self has an inherent essence or true nature, while others propose that the self is a constantly evolving and changing entity.

From a psychological standpoint, the self can be seen as a combination of various factors, including our thoughts, emotions, beliefs, and experiences. It is influenced by our environment, relationships, and personal development. However, it's important to note that the understanding of the self is subjective and can vary from person to person.

If you're interested in exploring this topic further, I encourage you to delve into philosophical and psychological literature, which can provide different perspectives and theories on the nature of the self. Additionally, you may find it helpful to engage in introspection and self-reflection to gain personal insights.

"""

long_quote_author = "Ecosia Chat"


print(f'"{long_quote.strip()}"', "--", long_quote_author)

#TODO: Break long_quote into paragraphs. Count the paragraphs. If there are more than one paragraph, use a different format than you do if there is one.