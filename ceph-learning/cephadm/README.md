# Cephadm notes

https://www.youtube.com/watch?v=4TLEg_OIU8M

https://docs.ceph.com/en/latest/cephadm/

https://docs.ceph.com/en/latest/cephadm/install/#cephadm-deploying-new-cluster

Trying this with Fedora Linux 39 and AlmaLinux 9

Ran into problems getting the static IP addresses to stick on Fedora, so back to AlmaLinux. Maybe Debian or Ubuntu next time.

```bash
vagrant ssh cephadm1

# Fedora does not need next two lines
dnf search release-ceph
sudo dnf install centos-release-ceph-reef

# Install cephadm
sudo dnf install -y cephadm

# Init the cluster
sudo cephadm bootstrap --mon-ip 192.168.56.51
exit
```

```text
Ceph Dashboard is now available at:

             URL: https://cephadm1:8443/
            User: admin
        Password: 6epz4xtg7u

Enabling client.admin keyring and conf on hosts with "admin" label
Saving cluster configuration to /var/lib/ceph/ada882b8-6937-11ef-bb23-080027228f22/config directory
Enabling autotune for osd_memory_target

Or, if you are only running a single cluster on this host:

        sudo /sbin/cephadm shell 
```

Enable telemetry?

```bash
vagrant ssh cephadm1
sudo cephadm shell
ceph telemetry on --license sharing-1-0
ceph telemetry enable channel perf
exit
```

Authorize ceph admin to login as root everywhere

```bash
# grep the Ceph key from root's authorized_keys on all systems
# ensure exactly one
THE_CEPH_KEY=$( cat /etc/ceph/ceph.pub | awk '{print $NF}' )
# for each host, grep and count matches
sudo grep $THE_CEPH_KEY ~root/.ssh/authorized_keys | wc -l
for S in cephadm{1..7}; do echo $S; ssh root@$S grep $THE_CEPH_KEY ~root/.ssh/authorized_keys | wc -l; done
# if one, yay
# if zero, add the key
ssh-copy-id -f -i /etc/ceph/ceph.pub root@cephadm7
# if more than one, remove all the matching keys
exit
```

Add remaining hosts to the cluster

```bash
vagrant ssh cephadm1
sudo cephadm shell
ceph orch host ls
ceph orch host add cephadm2
ceph orch host ls
exit
```

# Create OSDs

```bash
vagrant ssh cephadm1
sudo cephadm shell
ceph orch device ls
ceph orch apply osd --all-available-devices
exit
```

# Create a backup admin node

```bash
vagrant ssh cephadm1
sudo cephadm shell
ceph orch host label add cephadm2 _admin
exit
```

```bash
vagrant ssh cephadm2
sudo cephadm shell
ceph status
ceph orch host ls
ceph orch device ls
```

https://mattermost.sofree.us/sfs303/pl/fa6f7gkf73bq8xq3npyh4qzhue
