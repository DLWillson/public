# Markdown Tests

The above line is a header 1.

## Link Tests

The above line is a header 2.

This link should open in the same window: [Software Freedom School](https://www.sofree.us)

The below links should open in new tabs or windows.

- [Software Freedom School](https://www.sofree.us){:target="_blank"}
- [Software Freedom School](https://www.sofree.us){target="_blank"}

That doesn't work, at least in GitLab. Everything opens in a new tab. Let's try inline HTML.

- <a href="https://www.sofree.us">Software Freedom School</a>
- <a href="https://www.sofree.us" target="_blank">Software Freedom School</a>
